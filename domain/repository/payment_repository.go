package repository

import (
	"gitee.com/apinktara/payment/domain/model"
	"github.com/jinzhu/gorm"
)

type IPaymentRepository interface {
	InitTable() error
	FindPaymentByID(int64) (*model.Payment, error)
	CreatePayment(*model.Payment) (int64, error)
	DeletePaymentByID(int64) error
	UpdatePayment(*model.Payment) error
	FindAllPaymentByID() ([]model.Payment, error)

}

func NewPaymentRepository(db *gorm.DB) IPaymentRepository {
	return &PaymentRepository{mysqlDB: db}
}

type PaymentRepository struct {
	mysqlDB *gorm.DB
}

func (u *PaymentRepository) InitTable() error {
	return u.mysqlDB.CreateTable(model.Payment{}).Error
}
func (u *PaymentRepository) FindPaymentByID(PaymentID int64) (Payment *model.Payment, err error) {
	Payment = &model.Payment{}
	return Payment, u.mysqlDB.First(Payment, PaymentID).Error
}

func (u *PaymentRepository) CreatePayment(payment *model.Payment) (int64, error) {
	return payment.ID,u.mysqlDB.Update(payment).Error
}

func (u *PaymentRepository) DeletePaymentByID(cartID int64) error {
	return u.mysqlDB.Where("id =?", cartID).Delete(&model.Payment{}).Error
}

func (u *PaymentRepository) UpdatePayment(Payment *model.Payment) error {
	return u.mysqlDB.Model(Payment).Update(Payment).Error
}

func (u *PaymentRepository) FindAllPaymentByID() (PaymentAll []model.Payment, err error) {
	return PaymentAll, u.mysqlDB.Find(&PaymentAll).Error
}

