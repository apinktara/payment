package service

import (
	"gitee.com/apinktara/payment/domain/model"
	"gitee.com/apinktara/payment/domain/repository"
)

type IPaymentDataService interface {
	AddPayment(product *model.Payment) (int64, error)
	DeletePayment(int64) error
	UpdatePayment(*model.Payment) error
	FindPaymentByID(int64) (*model.Payment, error)
	FindAllPaymentByID() ([]model.Payment, error)

}

func NewPaymentDataService(productRepository repository.IPaymentRepository) IPaymentDataService {
	return &PaymentDataService{PaymentRspository: productRepository}
}

type PaymentDataService struct {
	PaymentRspository repository.IPaymentRepository
}

func (u *PaymentDataService) AddPayment(product *model.Payment) (int64, error) {
	return u.PaymentRspository.CreatePayment(product)
}

func (u *PaymentDataService) DeletePayment(productID int64) error {
	return u.PaymentRspository.DeletePaymentByID(productID)
}

func (u *PaymentDataService) UpdatePayment(product *model.Payment) error {
	return u.PaymentRspository.UpdatePayment(product)
}

func (u *PaymentDataService) FindPaymentByID(productID int64) (*model.Payment, error) {
	return u.PaymentRspository.FindPaymentByID(productID)
}

func (u *PaymentDataService) FindAllPaymentByID() ([]model.Payment, error) {
	return u.PaymentRspository.FindAllPaymentByID()
}


