package model

type Payment struct {
	ID int64 `gorm:"primary_key;not_null;auto_increment" json:"id"`

	PaymentName string `json:"payment_name"`
	PsymentSID string `json:"psyment_sid"`
	PaymentStatus bool `json:"payment_status"`
	PaymentImage string `json:"payment_image"`
}
