package main

import (
	"context"
	"fmt"
	"gitee.com/apinktara/cart/common"
	go_micro_service_product "gitee.com/apinktara/payment/product/proto/payment"
	"github.com/asim/go-micro/plugins/registry/consul/v3"
	opentracing2 "github.com/asim/go-micro/plugins/wrapper/trace/opentracing/v3"
	"github.com/asim/go-micro/v3"
	"github.com/asim/go-micro/v3/registry"
	"github.com/opentracing/opentracing-go"
)

func main() {

	consulRegistry := consul.NewRegistry(func(options *registry.Options) {
		options.Addrs = []string{
			"127.0.0.1:8500",
		}
	})
	//链路追追踪
	t, io, err := common.NewTracer("go.micro.service.payment", "127.0.0.1:6831")
	if err != nil {
		fmt.Println(err)
	}
	defer io.Close()
	opentracing.SetGlobalTracer(t)

	service := micro.NewService(
		micro.Name("go.micro.service.payment.client"),
		micro.Version("latest"),
		micro.Address("127.0.0.1:8085"),
		micro.Registry(consulRegistry),
		micro.WrapClient(opentracing2.NewClientWrapper(opentracing.GlobalTracer())),
	)

	productService := go_micro_service_product.NewProductService("go.micro.service.payment", service.Client())

	productAdd := &go_micro_service_product.ProductInfo{
		ProductName:        "imooc",
		ProductSku:         "cap",
		ProductPrice:       1.1,
		ProductDescription: "imooc-cap",
		ProductCategoryId:  1,
		ProductImage: []*go_micro_service_product.ProductImage{
			{
				ImageCode: "capimage01",
				ImageName: "cao-image01",
				ImageUrl:  "capimage01",
			},
			{
				ImageCode: "capimage02",
				ImageName: "cao-image02",
				ImageUrl:  "capimage02",
			},
			{
				ImageCode: "capimage03",
				ImageName: "cao-image03",
				ImageUrl:  "capimage03",
			},
		},
		ProductSize: []*go_micro_service_product.ProducSize{
			{
				SizeName: "cap-size",
				SizeCode: "cap-code",
			},
		},
		ProductSeo: &go_micro_service_product.ProductSeo{
			SeoCode:        "code",
			SeoTitle:       "title",
			SeoDescription: "description",
			SeoKeyword:     "keyword",
		},
	}

	response,err:=productService.AddProduct(context.TODO(),productAdd)
	if err!=nil {
		fmt.Println(err)
	}
	fmt.Println(response)

}
