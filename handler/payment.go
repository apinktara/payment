package handler

import (
	"context"
	"gitee.com/apinktara/common"
	"gitee.com/apinktara/payment/domain/model"
	"gitee.com/apinktara/payment/domain/service"
	. "gitee.com/apinktara/payment/proto/payment"
)

type Payment struct {
	PaymentDataService service.IPaymentDataService
}

func (h *Payment) AddPayment(ctx context.Context, request *PaymentInfo, response *PaymentID) error {
	cartAdd := &model.Payment{}
	if err := common.SwapTo(request, cartAdd); err != nil {
		return err
	}
	cartID, err := h.PaymentDataService.AddPayment(cartAdd)
	if err != nil {
		common.Debug(err)
	}
	response.PaymentId = cartID
	return nil
}

func (h *Payment) UpdatePayment(ctx context.Context, request *PaymentInfo, response *Response) error {
	payment := &model.Payment{}
	if err := common.SwapTo(request, payment); err != nil {
		common.Debug(err)
	}

	return h.PaymentDataService.UpdatePayment(payment)
}

func (h *Payment) DeletePaymentByID(ctx context.Context, request *PaymentID, response *Response) error {

	return h.PaymentDataService.DeletePayment(request.PaymentId)
}

func (h *Payment) FindPaymentByID(ctx context.Context, request *PaymentID, response *PaymentInfo) error {
	payment, err := h.PaymentDataService.FindPaymentByID(request.PaymentId)
	if err != nil {
		common.Debug(err)
	}
	return common.SwapTo(payment, response)
}

func (h Payment) FindAllPaymentByID(ctx context.Context, request *All, response *PaymentAll) error {
	cartAll, err := h.PaymentDataService.FindAllPaymentByID()
	if err != nil {
		return err
	}
	for _, v := range cartAll {
		paymentInfo := &PaymentInfo{}
		if err := common.SwapTo(v, paymentInfo); err != nil {
			return err
		}
		response.PaymentInfo = append(response.PaymentInfo, paymentInfo)
	}
	return nil
}
