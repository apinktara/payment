module gitee.com/apinktara/payment

go 1.13

require (
	gitee.com/apinktara/common v0.0.0-20220508064517-0cf30076edf4
	github.com/HdrHistogram/hdrhistogram-go v1.0.1 // indirect
	github.com/asim/go-micro/plugins/config/source/consul/v3 v3.7.0
	github.com/asim/go-micro/plugins/registry/consul/v3 v3.7.0
	github.com/asim/go-micro/plugins/wrapper/monitoring/prometheus/v3 v3.7.0 // indirect
	github.com/asim/go-micro/plugins/wrapper/ratelimiter/uber/v3 v3.7.0 // indirect
	github.com/asim/go-micro/plugins/wrapper/trace/opentracing/v3 v3.7.0
	github.com/asim/go-micro/v3 v3.7.1
	github.com/golang/protobuf v1.5.2
	github.com/hashicorp/consul/api v1.12.0 // indirect
	github.com/jinzhu/gorm v1.9.16
	github.com/opentracing/opentracing-go v1.2.0
	github.com/uber/jaeger-client-go v2.30.0+incompatible
	github.com/uber/jaeger-lib v2.4.1+incompatible // indirect
)

replace google.golang.org/grpc => google.golang.org/grpc v1.26.0
